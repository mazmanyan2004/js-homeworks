//1.Write the function add(3)(6), which returns the sum of these two numbers 

function add(a, b) {
    return  (b) =>{
        return a+b;
    }
}
alert(add(4)(5));

//2. Find the average age of users.

function getAverageAge(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i].age;
    }
    return sum / arr.length;
}

const users = [
    {
        gender: 'male',
        age: 24,
    },
    {
        gender: 'female',
        age: 21,
    },
    {
        gender: 'male',
        age: 36,
    }
];

console.log(getAverageAge(users));

//3. Write a function that gets an array and returns unique values with a new array
//For example:
//const result = findUniqueElements([10, 5, 6, 10, 6, 7]);
// result = [5, 7]
function findUniqueElements(arr) {
    for (let i = 0; i <= arr.length;) {
        if (arr.lastIndexOf(arr[i]) < 0) break;
        else if (i !== arr.lastIndexOf(arr[i])) {
            while (i !== arr.lastIndexOf(arr[i])) {
                arr.splice(arr.lastIndexOf(arr[i]), 1);
            }
            arr.splice(i, 1);
        }
        else i++;
    }
    return arr;
};
console.log(findUniqueElements([10, 5, 6, 10, 6, 7]));

function findUniqueElements(arr){
    arr.forEach((element, index) => {
        for(let i=index+1;i<arr.length;i++){
            if(element==arr[i]){
                arr.splice(i, 1);
                arr.splice(index, 1);
            }
        }
    }); 
    return arr;
};



//4. Create a createCounter function that returns one more value from each call and starts at 0

function createCounter(){
    let a=0;
    return () => {
        return a++;
    }
}
const getCount = createCounter()
console.log (getCount()); // 0:
console.log (getCount()); // 1:
console.log (getCount()); // 2: